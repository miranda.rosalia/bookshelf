## <a name="api-exercises">API Exercises</a>

### User Story

```text
As a user, I want to administrate my online bookshelf. The system has to have support to keep updated the information
related to books like title, format, author, language, description, publication year and rating. It must allow to insert,
delete and update book information.
It's also required to list books using some filters like title, format, etc.
```
#### Exercises

1) Create a new text file and list all test cases that you would execute to test the story.
2) Prioritize and choose some candidates to be automated.

| Test Name                       | Description                                    | Priority | Automated test name            |
|---------------------------------|------------------------------------------------|----------|--------------------------------|
| Búsqueda por Id                 | Se buscará por el id de un libro               | alta     | sussessfulSearchBookById()     |
| Búsqueda por Titulo             | Se buscará por el titulo de un libro           | alta     |                                |
| Búsqueda por Formato            | Se buscará por el formato de un libro          | Baja     |                                |
| Búsqueda por Autor              | Se buscará por el autor de un libro            | alta     | sussessfulSearchBookByAuthor() |
| Búsqueda por Idioma             | Se buscará por el idioma de un libro           | alta     |                                |
| Búsqueda por Descripcion        | Se buscará por la descripcion de un libro      | Baja     |                                |
| Búsqueda por año de publicacion | Se buscará por el año de publicacion del libro | Baja     |                                |
| Búsqueda por Calificacion       | Se buscará por la Calificacion                 | Baja     |                                |


| Test Name                      | Description                                                             | Priority | Automated test name                 |
|--------------------------------|-------------------------------------------------------------------------|----------|-------------------------------------|
| Actualizar  Titulo             | Se buscará por el id de un libro y se actualizará el titulo             | media    | sussessfulUpdateBookRecordByTitle() |
| Actualizar  Formato            | Se buscará por el por el id de un libro y se actualizará el formato     | media    |                                     |
| Actualizar  Autor              | Se buscará por el id de un libro y se actualizará el  autor             | media    |                                     |
| Actualizar  Idioma             | Se buscará por el id de un libro y se actualizará el idioma             | media    |                                     |
| Actualizar  Descripcion        | Se buscará por id de un libro y se actualizará la descripcion           | media    |                                     |
| Actualizar  año de publicacion | Se buscará por el id de un libro y se actualizará el año de publicacion | media    |                                     |
| Actualizar  Calificacion       | Se buscará por el id de un libro y se actualizará la Calificacion       | media    |                                     |

| Test Name                      | Description                                                 | Priority | Automated test name     |
|--------------------------------|-------------------------------------------------------------|----------|-------------------------|
| Listar por  Titulo             | Se listará todos los libros con el mismo titulo             | alta     |                         |
| Listar por  Formato            | Se listará todos los libros con el mismo formato            | Baja     |                         |
| Listar por  Autor              | Se listará todos los libros del mismo autor                 | alta     |                         |
| Listar por  Idioma             | Se listará todos los libros del idioma                      | Baja     |                         |
| Listar por  año de publicacion | Se listará todos los libros con el mismo año de publicacion | Baja     |                         |
| Listar por  Calificacion       | Se listará todos los libros con la misma  Calificacion      | Baja     |                         |
| Listar todos los libros        | Se listará todos los libros con la misma  Calificacion      | media    | sussessfulSearchBooks() |


| Test Name                                       | Description                                                                                            | Priority | Automated test name          |
|-------------------------------------------------|--------------------------------------------------------------------------------------------------------|----------|------------------------------|
| Ingresar un nuevo libro con un id Caso exitoso  | Se creará un libro con una identificacion unica                                                        | alta     | sussessfulAddABookRecord()   |
| Ingresar un nuevo libro sin un id Caso exitoso  | Se creará un libro con una identificacion unica aleatoria                                              | alta     |                              |
| Ingresar un nuevo libro  con un id Caso Fallido | Se verificará que no se puede ingresar un nuevo libro con una identificacion ya cargada anteriormente. | alta     |                              |
| Eliminar un libro con un id                     | Se eliminará un libro utilizando un id existente en la bbdd                                            | alta     | sussessfulDeleteBookRecord() |





### Requirements for execution
JDK (Java Development Kit) 11.0.18 
IDE IntelliJ IDEA 2023.1 (Community Edition)




