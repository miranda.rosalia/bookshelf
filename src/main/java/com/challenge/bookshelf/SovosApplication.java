package com.challenge.bookshelf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SovosApplication {

	public static void main(String[] args) {
		SpringApplication.run(SovosApplication.class, args);
	}

}
