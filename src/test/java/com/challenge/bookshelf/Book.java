package com.challenge.bookshelf;

import io.restassured.response.Response;
import lombok.Data;


@Data
public class Book {
    public Book() {

    }


    public String getId() { return id;}

    public String getTitle() { return title;}

    public String getFormat() { return format;}

    public String getAuthor() { return author;}

    public String getLanguage() { return language;}

    public String getPubYear() { return pubYear;}

    public String getDescription() { return description;}

    public Integer getRating() { return rating;}

    private  String id;

    public void setId(String id) {
        this.id = id;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setPubYear(String pubYear) {
        this.pubYear = pubYear;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    private  String title;
    private  String format;
    private  String author;
    private  String language;
    private  String pubYear;
    private  String description;
    private  Integer rating;

    public Book(String id, String title, String format, String author, String language, String pubYear, String description, Integer rating) {
        this.id = id;
        this.title = title;
        this.format = format;
        this.author = author;
        this.language = language;
        this.pubYear = pubYear;
        this.description = description;
        this.rating = rating;
    }
    public Book(Response response){
        this.id = response.jsonPath().getString("id[1]");
        this.title =  response.jsonPath().getString("title[2]");
        this.format = response.jsonPath().getString("format[3]");
        this.author = response.jsonPath().getString("author[4]");
        this.language =  response.jsonPath().getString("language[5]");
        this.pubYear = response.jsonPath().getString("pubYear[6]");
        this.description =response.jsonPath().getString("description[7]");
        this.rating = response.jsonPath().getInt("rating[8]");

    }
}
