package com.challenge.bookshelf;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;

public class BookshelfTest {
    final static String baseURL="http://localhost:3001/"; // Configuration

    @Test
    public void sussessfulSearchBooks(){ searchBookAll();}
    @Test
    public void sussessfulSearchBookById(){
        searchBookBy("id","1");
    }
    @Test
    public void sussessfulSearchBookByAuthor(){
        searchBookBy("author","Edgar Allan Poe");
    }
    @Test
    public void sussessfulAddABookRecord() throws Exception{
        Book book = new Book("3340","De Muchas cosas","Paperback","Yuval Noah","Spanish","2022","",4);
        addABookRecord(book);
        searchBookBy("id",book.getId());
    }
    @Test
    public void sussessfulUpdateBookRecordByTitle() throws Exception{
        updateBookRecord("300","title"," tituloo");
    }
    @Test
    public void sussessfulDeleteBookRecord() throws Exception{
        deleteBookRecord("3340");
    }
    private List<Book> searchBookAll() {
        List <Book> listBooks  =  given()
                .header("Content-type", "application/json")
                .when()
                .get(baseURL+"books")
                .then()
                .extract()
                .body()
                .jsonPath().getList(".",Book.class);
        return listBooks;
    }
    private  void searchBookBy(String parameterName, String value) {
        given().queryParam(parameterName, value)
                .when().get(baseURL+"books")
                .then()
                .statusCode(200)
                .log().body();
         }
    private void addABookRecord(Book book) throws Exception  {

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(book)
                .when()
                .post(baseURL+"books")
                .then()
                .statusCode(201)
                .extract().response();

        Assertions.assertEquals(book.getTitle(), response.jsonPath().getString("title"));
        Assertions.assertEquals(book.getAuthor(), response.jsonPath().getString("author"));
        Assertions.assertEquals(book.getId(), response.jsonPath().getString("id"));
    }
    private void updateBookRecord(String id , String parameterName, String value) throws JsonProcessingException {
        String body = "{\n" +
                '"'+parameterName + '"' +":" +'"'+ value+'"' +"\n}";

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(body)
                .when()
                .patch(baseURL+"books/"+id)
                .then()
                .statusCode(200)
                .extract().response();
        Assertions.assertEquals(value, response.jsonPath().getString(parameterName));
    }
    private  void deleteBookRecord(String id) throws JsonProcessingException {
        Response response = given()
                .header("Content-type", "application/json")
                .when()
                .delete(baseURL+"books/"+id)
                .then()
                .statusCode(200)
                .extract().response();
    }

}
